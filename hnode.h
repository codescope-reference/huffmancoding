#ifndef HNODE_H
#define HNODE_H

#include <stdlib.h>

typedef struct HNode {
  struct HNode *left;
  struct HNode *right;
  struct HNode *parent;
  size_t weight;
  unsigned char byte_value;
} HNode;


bool is_leaf(HNode *node);
void free_tree(HNode *root);
void print_tree(HNode *root, int level);

#endif
