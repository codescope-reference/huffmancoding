#include "writer.h"

void write_bit(Writer *writer, bool bit) {
  if (writer->bit_position == 0) {
    if (writer->size + 1 > writer->capacity) {
      if (writer->capacity == 0) {
        writer->capacity = 8;
      } else {
        writer->capacity *= 2;
      }
      writer->items = realloc(writer->items, writer->capacity);
    }

    writer->items[writer->size] = 0;
  }

  writer->items[writer->size] |= bit << (7 - writer->bit_position);
  writer->bit_position = (writer->bit_position + 1) % 8;
  if (writer->bit_position == 0) {
    writer->size++;
  }
}


void print_bits(Writer *writer) {
  for (size_t i = 0; i < writer->size; ++i) {
    for (int j = 7; j >= 0; --j) {
      fprintf(stderr, "%d", (writer->items[i] >> j) & 1);
    }
  }
  fprintf(stderr, "\n");
}
