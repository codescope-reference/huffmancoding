#include "pqueue.h"



void pqueue_insert(PQueue *pq, HNode * item) {
  if (pq->size + 1 > pq->capacity) {
    if (pq->capacity == 0) {
      pq->capacity = 8;
    } else {
      pq->capacity *= 2;
    }
    pq->data = realloc(pq->data, pq->capacity * sizeof(HNode *));
  }

  for (int i = pq->size -1; i >= 0; --i) {
    if (item->weight < pq->data[i]->weight) {
      // First move everything above i one step up
      // Then insert the item at i
      // a b c d 
      // a b b c d 
      for (int j = pq->size - 1; j >= i + 1; --j) {
        pq->data[j + 1] = pq->data[j];
      }
      pq->data[i + 1] = item;
      pq->size++;

      return;
    }
  }
  for (int j = pq->size - 1; j >= 0; --j) {
    pq->data[j + 1] = pq->data[j];
  }
  pq->data[0] = item;
  pq->size++;
}

HNode * pqueue_pop(PQueue *pq) {
  assert(pq->size > 0 && "Cannot pop from empty queue");
  HNode *item = pq->data[pq->size - 1];
  pq->size--;
  return item;
}

