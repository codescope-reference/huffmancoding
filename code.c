#include "code.h"


bool temp_stack[256] = {0};
int stack_top = 0;

void traverse(Codebook *cb, HNode *current, int level, size_t current_code) {
  if (!is_leaf(current)) {
    temp_stack[stack_top++] = 0;
    traverse(cb, current->left, level + 1, (current_code << 1) | 0);
    stack_top--;
  }

  if (is_leaf(current)) {

    Codeword cw = {0};
    cw.byte_value = current->byte_value;
    cw.code_value = current_code;
    cw.code_length = level;
    for (int i = 0; i < stack_top; ++i) {
      cw.temp[i] = temp_stack[i];
    }
    cw.temp_length = stack_top;

    da_append(cb, cw);
    return;
  }

  if (!is_leaf(current)) {
    temp_stack[stack_top++] = 1;
    traverse(cb, current->right, level + 1, (current_code << 1) | 1);
    stack_top--;
  }
}

Codebook create_codebook(HNode *root) {
  Codebook cb = {0};

  traverse(&cb, root, 0, 0);

  return cb;
}

void save_codebook(Codebook *cb, const char *filename) {
  FILE *f = fopen(filename, "wb");

  assert(cb->size < 512);
  fwrite(&cb->size, sizeof(size_t), 1, f);
  for (size_t i = 0; i < cb->size; ++i) {
    fwrite(&cb->items[i].byte_value, sizeof(char), 1, f);
    fwrite(&cb->items[i].code_value, sizeof(size_t), 1, f);
    fwrite(&cb->items[i].code_length, sizeof(int), 1, f);
  }

  fclose(f);
}

Codebook load_codebook(const char *filename) {
  Codebook cb = {0};

  FILE *f = fopen(filename, "rb");

  size_t n_elements = 0;
  fread(&n_elements, sizeof(size_t), 1, f);
  assert(n_elements < 512);
  cb.items = malloc(n_elements * sizeof(Codeword));
  cb.capacity = n_elements;
  for (size_t i = 0; i < n_elements; ++i) {
    Codeword cw = {0};
    fread(&cw.byte_value, sizeof(char), 1, f);
    fread(&cw.code_value, sizeof(size_t), 1, f);
    fread(&cw.code_length, sizeof(int), 1, f);

    da_append(&cb, cw);
  }

  fclose(f);

  return cb;
}



HNode *rebuild_tree(Codebook *cb) {
  HNode *root = calloc(1, sizeof(HNode));


  for (size_t i = 0; i < cb->size; ++i) {
    HNode *current = root;
    Codeword cw = cb->items[i];
    for (size_t j = 0; j < cw.code_length; ++j) {
      bool bit_val = (cw.code_value >> (cw.code_length - j - 1)) & 1;

      if (bit_val) {
        // Go right
        if (current->right == NULL) {
          current->right = calloc(1, sizeof(HNode));
        }
        current = current->right;
      } else {
        // Go left
        if (current->left == NULL) {
          current->left = calloc(1, sizeof(HNode));
        }
        current = current->left;
      }
    }

    current->byte_value = cw.byte_value;
  }


  return root;
}


