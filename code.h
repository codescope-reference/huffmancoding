#ifndef CODE_H
#define CODE_H

#include <assert.h>
#include "hnode.h"
#include "utils.h"

typedef struct Codeword {
  size_t code_value;
  size_t code_length;
  unsigned char byte_value;
  bool temp[256];
  int temp_length;
} Codeword;

typedef struct Codebook {
  Codeword *items;
  size_t size;
  size_t capacity;
} Codebook;


void traverse(Codebook *cb, HNode *current, int level, size_t current_code);
Codebook create_codebook(HNode *root);
void save_codebook(Codebook *cb, const char *filename);
Codebook load_codebook(const char *filename);
HNode *rebuild_tree(Codebook *cb);

#endif
