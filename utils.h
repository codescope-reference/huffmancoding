#ifndef UTILS_H
#define UTILS_H


#define da_append(a, v) do { \
  if ((a)->size >= (a)->capacity) { \
    if ((a)->capacity == 0) { \
      (a)->capacity = 8; \
    } else { \
      (a)->capacity *= 2; \
    } \
    (a)->items = realloc((a)->items, (a)->capacity * sizeof(Codeword)); \
  } \
  (a)->items[(a)->size++] = (v); \
} while (0)

#endif
