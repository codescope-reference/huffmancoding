#!/bin/bash
set -e

CFLAGS="-Wall -Wextra -Werror"
gcc -o main main.c -ggdb $CFLAGS
gcc -o main_debug main.c -ggdb -DDEBUG $CFLAGS



for f in $(ls tests/input*.txt);
do
  codebook_name=$(echo $f | sed 's/input/codebook/' | sed 's/txt/bin/')
  echo -n "Compressing $f"
  compressed_name=$(echo $f | sed 's/input/compressed/')
  ./main -c $codebook_name $f > $compressed_name
  echo "...OK"

  echo -n "Decompressing $compressed_name"
  decompressed_name=$(echo $compressed_name | sed 's/compressed/decompressed/')
  ./main -d $codebook_name $compressed_name > $decompressed_name
  echo "...OK"


  echo -n "Diffing $f and $decompressed_name"
  # Error if diff is not empty
  [ "$(diff $f $decompressed_name)" ] && (echo "...FAIL" && echo "Error: Files are different" && exit 1)
  echo "...OK"


  # Calculate compression ratio
  original_size=$(stat -c %s $f)
  compressed_size=$(stat -c %s $compressed_name)
  ratio=$(echo "scale=2; $original_size / $compressed_size" | bc)
  echo "$original_size bytes -> $compressed_size bytes: Compression ratio: $ratio"


  colwidth=$(tput cols)
  printf "%0.s#" $(seq 1 $colwidth)


done

echo "All tests passed"
