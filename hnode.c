#include "hnode.h"

bool is_leaf(HNode *node) {
  return node->left == NULL && node->right == NULL;
}

void free_tree(HNode *root) {
  if (root == NULL) return;

  free_tree(root->left);
  free_tree(root->right);

  free(root);
}

void print_tree(HNode *root, int level) {
  if (root == NULL) return;

  print_tree(root->right, level + 1);

  for (int i = 0; i < level; ++i) {
    printf("--");
  }
  printf("%c\n", root->byte_value);

  print_tree(root->left, level + 1);
}
