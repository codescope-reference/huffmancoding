#ifndef WRITER_H
#define WRITER_H

typedef struct Writer {
  unsigned char *items;
  size_t size;
  size_t capacity;

  size_t bit_position;
} Writer;


void write_bit(Writer *writer, bool bit);
void print_bits(Writer *writer);

#endif
