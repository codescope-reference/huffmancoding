#include "bitreader.h"


void read_bit(BitReader *reader, FILE *file) {
  if (reader->bit_position == 0) {
    unsigned char c = fgetc(file);
    reader->current_byte = c;
  }

  bool bit = (reader->current_byte >> (7 - reader->bit_position)) & 1;
  da_append(reader, bit);
  reader->bit_position = (reader->bit_position + 1) % 8;
}

void read_file_to_bits(BitReader *reader, const char *filename) {
  FILE *file = fopen(filename, "r");

  int result = fseek(file, 0, SEEK_END);
  if (result != 0) {
    fprintf(stderr, "Error seeking file %s\n", filename);
    exit(1);
  }
  size_t size = ftell(file);
  result = fseek(file, 0, SEEK_SET);
  if (result != 0) {
    fprintf(stderr, "Error seeking file %s\n", filename);
    exit(1);
  }

  // Read tag
  for (int i = 0; i < 9; ++i) {
    fgetc(file);
  }

  // Read bit length
  int bit_length = 0;
  for (int i = 0; i < 4; ++i) {
    unsigned char c = fgetc(file);
    bit_length += c << (24 - 8 * i);
  }
  // fscanf(file, "%d", &bit_length);

  for (size_t i = ftell(file); i < size; ++i) {
    for (int b = 0; b < 8; ++b) {
      read_bit(reader, file);
    }
  }

  fclose(file);
  reader->size = bit_length;
}
