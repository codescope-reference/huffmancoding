import numpy as np
from subprocess import run


prefix = "input_"
size = 1000

a = np.random.choice(["a", "b"], size=(size,), p=[0.8, 0.2])
with open(f"{prefix}simple.txt", "w") as f:
    f.write("".join(a))

a = np.random.choice(["a", "b", "c", "d"], size=(size,), p=[0.5, 0.2, 0.2, 0.1])
with open(f"{prefix}simple_4.txt", "w") as f:
    f.write("".join(a))

a = np.random.choice(["a", "b"], size=(size,), p=[0.5, 0.5])
with open(f"{prefix}equal_2.txt", "w") as f:
    f.write("".join(a))

a = np.random.choice(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"], size=(size,), p=[0.2, 0.05, 0.05, 0.1, 0.1, 0.2, 0.05, 0.05, 0.1, 0.1])
with open(f"{prefix}test_10.txt", "w") as f:
    f.write("".join(a))



n_elem = 100
probs = np.random.rand(n_elem)
probs /= np.sum(probs)
a = np.random.choice(range(n_elem), size=(size,), p=probs)
with open(f"{prefix}random.txt", "w") as f:
    f.write("".join(map(str, a)))

n_elem = 100
probs = np.random.rand(n_elem)
probs += 50.0
probs /= np.sum(probs)
a = np.random.choice(range(n_elem), size=(size,), p=probs)
with open(f"{prefix}random_biased.txt", "w") as f:
    f.write("".join(map(str, a)))



size = 10000
a = np.random.choice(["a", "b"], size=(size,), p=[0.8, 0.2])
with open(f"{prefix}simple_verylong.txt", "w") as f:
    f.write("".join(a))


run("head -c 100 /dev/urandom > input_urandom.txt", shell=True)
run("head -c 2048 /dev/urandom > input_urandom_big.txt", shell=True)
