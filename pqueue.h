#ifndef PQUEUE_H
#define PQUEUE_H

#include "hnode.h"

typedef struct PQueue {
  HNode **data;
  size_t size;
  size_t capacity;
} PQueue;


void pqueue_insert(PQueue *pq, HNode * item);
HNode * pqueue_pop(PQueue *pq);

#endif
