#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>

#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"



#include "pqueue.h"
#include "hnode.h"
#include "writer.h"
#include "bitreader.h"
#include "utils.h"
#include "code.h"

#include "pqueue.c"
#include "hnode.c"
#include "writer.c"
#include "bitreader.c"
#include "code.c"


typedef struct Count {
  unsigned char key;
  size_t value;
} Count;


Count *count_bytes(const char* filename) {
  Count *hm = NULL;

  FILE *file = fopen(filename, "r");
  fseek(file, 0, SEEK_END);
  size_t size = ftell(file);
  fseek(file, 0, SEEK_SET);

  for (size_t i = 0; i < size; i++) {
    unsigned char current_char = fgetc(file);

    if (hmgeti(hm, current_char) == -1) {
      hmput(hm, current_char, 1);
    } else {
      unsigned char current_value = hmget(hm, current_char);
      hmput(hm, current_char, current_value + 1);
    }
  }

  fclose(file);
  return hm;
}


char *read_to_string(const char *filename, size_t *size) {
  FILE *file = fopen(filename, "r");
  fseek(file, 0, SEEK_END);
  *size = ftell(file);
  fseek(file, 0, SEEK_SET);

  char *buffer = malloc(*size + 1);
  fread(buffer, 1, *size, file);
  buffer[*size] = '\0';

  fclose(file);
#ifdef DEBUG
  printf("Read %ld bytes in read_to_string\n", *size);
#endif
  return buffer;
}



void decompress(const char *codebook_file, const char* compressed_file) {
  Codebook cb = load_codebook(codebook_file);

  BitReader reader = {0};
  read_file_to_bits(&reader, compressed_file);

#ifdef DEBUG
  fprintf(stderr, "Reader reads %ld bits:\n", reader.size);
  for (size_t i = 0; i < reader.size; ++i) {
    fprintf(stderr, "%d", reader.items[i]);
  }
  fprintf(stderr, "\n");
#endif


  HNode *root = rebuild_tree(&cb);

  size_t idx = 0;
  int cap = reader.size / 8 + 1;
  char *decoded = malloc(cap);
  size_t current_buffer_size = cap;
  for (size_t i = 0; i < reader.size; ) {
    HNode *current = root;

    while (!is_leaf(current)) {
      assert(i < reader.size);
      bool bit = reader.items[i++];
      if (bit) {
        current = current->right;
      } else {
        current = current->left;
      }
    }
    if (idx >= current_buffer_size) {
      current_buffer_size *= 2;
      decoded = realloc(decoded, current_buffer_size);
      assert(decoded != NULL);
    }
    decoded[idx++] = current->byte_value;
  }
  decoded[idx] = '\0';
#ifdef DEBUG
  fprintf(stderr, "Got %ld bytes\n", idx);
#endif

  for (size_t i = 0; i < idx; ++i) {
    printf("%c", decoded[i]);
  }

  free(decoded);

  free_tree(root);
}

void print_binary(size_t n) {
  for (int i = 0; i < 8; ++i) {
    printf("%ld", (n >> (7 - i)) & 1);
  }
}

void print_bytes(void *p, size_t len)
{
    size_t i;
    for (i = 0; i < len; ++i)
        printf("%02X", ((unsigned char*)p)[i]);
}

void compress(const char *codebook_file, const char * file) {
  Count *hm = count_bytes(file);
  if (hmlen(hm) == 0) {
    printf("ERROR: File is empty\n");
    exit(1);
  }

  size_t cap = hmlen(hm) * hmlen(hm) * sizeof(HNode);
  size_t size = 0;
  HNode *nodes = malloc(cap);

  for (int i = 0; i < hmlen(hm); ++i) {
    HNode node = {0};
    node.byte_value = hm[i].key;
    node.weight = hm[i].value;
    nodes[size++] = node;
  }

  PQueue pq = {0};
  for (size_t i = 0; i < size; ++i) {
    pqueue_insert(&pq, &nodes[i]);
  }

  while (pq.size > 1) {
    HNode *a = pqueue_pop(&pq);
    HNode *b = pqueue_pop(&pq);

    HNode node = {0};
    node.left = a;
    node.right = b;
    node.weight = a->weight + b->weight;
    assert(size < cap && "TODO: Dynamic resizing");
    nodes[size++] = node;

    pqueue_insert(&pq, &nodes[size - 1]);
  }

  HNode *root = pqueue_pop(&pq);
  assert(pq.size == 0);


  // Create a codeword table
  Codebook cb = create_codebook(root);
  save_codebook(&cb, codebook_file);

  size_t len_contents;
  char *contents = read_to_string(file, &len_contents);

  Writer writer = {0};
  size_t n_bits = 0;
  for (size_t char_index = 0; char_index < len_contents; ++char_index) {
    unsigned char current_char = contents[char_index];
    Codeword code_word = {0};
    bool found = false;
    for (size_t cw_index = 0; cw_index < cb.size; ++cw_index) {
      if (cb.items[cw_index].byte_value == current_char) {
        code_word = cb.items[cw_index];
        found = true;
        break;
      }
    }
    assert(found && "Cannot find byte in codebook");

    for (size_t bit_index = 0; bit_index < code_word.code_length; ++bit_index) {
      bool bit = (code_word.code_value >> (code_word.code_length - bit_index - 1)) & 1;
      assert(bit == code_word.temp[bit_index]);
      write_bit(&writer, bit);
      n_bits++;
    }

  }

  size_t extra_bits = 0;
  while (writer.bit_position != 0) {
    write_bit(&writer, 0);
    extra_bits++;
  }
  if (writer.size != (n_bits + extra_bits) / 8) {
    fprintf(stderr, "ERROR: Writer size is not equal to n_bits / 8\n");
    fprintf(stderr, "Writer size: %ld\n", writer.size);
    fprintf(stderr, "n_bits / 8: %ld\n", n_bits / 8);
    exit(1);
  }
  assert(writer.size == (n_bits + extra_bits) / 8);
#ifdef DEBUG
  fprintf(stderr, "Writer writes:\n");
  print_bits(&writer);
  fprintf(stderr, "\n");
  fprintf(stderr, "Number of bits: %ld\n", n_bits);
#endif

  printf("codescope");
  assert(n_bits < 0xFFFFFFFF);
  for (int i = 0; i < 4; ++i) {
    printf("%c", (unsigned char)((n_bits >> (24 - i * 8)) & 0xFF));
  }
  for (size_t i = 0; i < writer.size; ++i) {
    printf("%c", writer.items[i]);
  }
#ifdef DEBUG
  fprintf(stderr, "\n");
#endif

  free(contents);
}

void print_usage(char *argv[]) {
  printf("For compression:\n");
  printf("Usage: %s -c <codebook-file> <file-to-compress>\n", argv[0]);
  printf("For decompression:\n");
  printf("Usage: %s -d <codebook-file> <file-to-compress>\n", argv[0]);
}

int main(int argc, char *argv[]) {
  if (argc < 4) {
    print_usage(argv);
    return 1;
  }

  if (argc == 4 && strcmp(argv[1], "-d") == 0) {
    decompress(argv[2], argv[3]);
    return 0;
  } else if (argc == 4 && strcmp(argv[1], "-c") == 0) {
    compress(argv[2], argv[3]);
    return 0;
  } else {
    printf("Invalid arguments\n");
    print_usage(argv);
    return 1;
  }


  return 0;
}
