#ifndef READER_H
#define READER_H

#include "utils.h"

typedef struct BitReader {
  bool *items;
  size_t size;
  size_t capacity;

  char current_byte;
  size_t bit_position;
} BitReader;


void read_bit(BitReader *reader, FILE *file);
void read_file_to_bits(BitReader *reader, const char *filename);

#endif
