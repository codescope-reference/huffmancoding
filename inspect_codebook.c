#include <stdio.h>
#include <stdbool.h>
#include "code.h"
#include "code.c"

#include "hnode.h"
#include "hnode.c"

int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("Usage: %s <codebook-file>\n", argv[0]);
    return 1;
  }


  Codebook cb = load_codebook(argv[1]);


  for (int i = 0; i < cb.size; ++i) {
    printf("'%03d': ", (unsigned char)cb.items[i].byte_value);
    for (int j = 0; j < cb.items[i].code_length; ++j) {
      printf("%ld", cb.items[i].code_value >> (cb.items[i].code_length - j - 1) & 1);
    }
    printf("\n");
  }
}
